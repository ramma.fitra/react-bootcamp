import React from "react";
import { Route, Switch } from "react-router-dom"; 
import LandingPages from "../pages/LandingPages";
import FindJobs from "../pages/FindJobs";
import BrowseCompanies from "../pages/BrowseCompanies";
import NavBar from "../components/NavBar"

const Routes = () => {
  return (
    <div>
      <NavBar/>
      <Switch>
        <Route path="/" exact component={LandingPages}/>
        <Route path="/FindJobs" exact component={FindJobs}/>
        <Route path="/BrowseCompanies" exact component={BrowseCompanies}/>
      </Switch>
    </div>
   
  )
}

export default Routes