import React from 'react';
import { Layout, Menu } from 'antd';

import './NavBar.css';

const { Header } = Layout;

const NavBar = () => {
  return (
    <Header className="navbar">
      <div className="logo">Your Logo</div>
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']} className="menu">
        <Menu.Item key="1">Home</Menu.Item>
        <Menu.Item key="2">About</Menu.Item>
        <Menu.Item key="3">Services</Menu.Item>
        <Menu.Item key="4" className="menu-item-right">Contact</Menu.Item>
      </Menu>
    </Header>
  );
};

export default NavBar;
