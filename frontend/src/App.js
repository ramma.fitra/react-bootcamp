import LandingPages from "./pages/LandingPages";
import FindJobs from "./pages/FindJobs";
import BrowseCompanies from "./pages/BrowseCompanies";


function App() {
  return (
    <div className="App">

      <NavBar />

      <LandingPages/>
      <FindJobs/>
      <BrowseCompanies/>
    </div>
  );
}

export default App;
