import React from "react";
import {Link} from "react-router-dom";
import Styles from "./styles/NavBar.module.css"
import Logo from "../assets/Logo2.png"
import {Button, Divider} from "antd"

const NavBar = () => {
    return (
        <div className={Styles.header}>
            <div>
                <img src={Logo} alt="Logo"/>
            </div>
            <ul className={Styles.topmenu}>
                <li><Link to="/">LandingPages</Link></li>
                <li><Link to="/FindJobs">FindJobs</Link></li>
                <li><Link to="/BrowseCompanies">BrowserCompanies</Link></li>
                <li><Button type="link">Login</Button></li>
                <li style={{ height: "48px" }}><Divider type="vertical" style={{ height: "100%" }} /></li>
                <li><Button type="primary">SignUp</Button></li>
            </ul>
        </div>
        
    )
  }
  
export default NavBar



